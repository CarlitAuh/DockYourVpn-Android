package com.example.dyv.dockyourvpn.model;

import java.util.Date;
import java.util.List;

public class User {

    private String mFirstName;
    private String mLastName;
    private Date mBirthday;
    private String mMail;
    private List<Offer> mOffer;

    public String getMail() {
        return mMail;
    }

    public void setMail(String mail) {
        mMail = mail;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public Date getBirthday() {
        return mBirthday;
    }

    public void setBirthday(Date birthday) {
        mBirthday = birthday;
    }

    public List<Offer> getOffer() {
        return mOffer;
    }

    public void setOffer(List<Offer> offer) {
        mOffer = offer;
    }
}
