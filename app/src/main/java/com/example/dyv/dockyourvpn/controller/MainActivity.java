package com.example.dyv.dockyourvpn.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dyv.dockyourvpn.R;
import com.example.dyv.dockyourvpn.model.User;

public class MainActivity extends AppCompatActivity {

    /* Variable rattaché à ma vue Activity Main */
    private TextView mWelcomeMessage;
    private EditText mMailLogin;
    private EditText mPasswordLogin;
    private Button mButtonLogin;
    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* Setter la vue qu'on va utiliser */
        setContentView(R.layout.activity_main);

        /* Rattacher les éléments de ma vue aux variables créer afin de les manipuler */
        mWelcomeMessage = (TextView) findViewById(R.id.activity_main_welcome_message);
        mMailLogin = (EditText) findViewById(R.id.activity_main_mail_login);
        mPasswordLogin = (EditText) findViewById(R.id.activity_main_password_login);
        mButtonLogin = (Button) findViewById(R.id.activity_main_button_login);

        /* Rendre le bouton non validable */
        mButtonLogin.setEnabled(false);

        /* Comportement du bouton si changement lorsque l'on tape */
        mMailLogin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mButtonLogin.setEnabled(s.toString().length() != 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        /* Comportement du bouton lorsqu'on clique */
        mButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toastMsg("Connexion Réussie");
                Intent homeActivity = new Intent(MainActivity.this, HomePageActivity.class);
                startActivity(homeActivity);
            }
        });
    }

    public void toastMsg(String msg){
        Toast.makeText(this,msg,Toast.LENGTH_LONG).show();
    }
}
