package com.example.dyv.dockyourvpn.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dyv.dockyourvpn.R;
import com.example.dyv.dockyourvpn.model.Offer;

import java.util.List;

/**
 * Created by tcarlier on 24/05/2018.
 */

public class OfferAdapter extends RecyclerView.Adapter<MyViewHolder>{

    List<Offer> mOfferList;

    public OfferAdapter(List<Offer> offerList) {
        mOfferList = offerList;
    }

    //cette fonction permet de créer les viewHolder
    //et par la même indiquer la vue à inflater (à partir des layout xml)
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int itemType){
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_cards,viewGroup,false);
        return new MyViewHolder(view);
    }

    //c'est ici que nous allons remplir notre cellule avec le texte/image de chaque MyObjects
    public void onBindViewHolder(MyViewHolder myViewHolder, int position){
        Offer offer = mOfferList.get(position);
        myViewHolder.bind(offer);
    }

    public int getItemCount(){
        return mOfferList.size();
    }
}
