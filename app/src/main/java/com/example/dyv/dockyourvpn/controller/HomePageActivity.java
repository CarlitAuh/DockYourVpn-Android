package com.example.dyv.dockyourvpn.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dyv.dockyourvpn.R;

public class HomePageActivity extends AppCompatActivity {

    /* Variable rattaché à ma vue Activity Main */
    private TextView mMessageConnection;
    private Button mDashboard;
    private Button mOffer;
    private Button mAccount;
    private Button mContact;
    private ImageView mImageDYV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* Setter la vue */
        setContentView(R.layout.activity_home_page);

        /* Rattacher les éléments de ma vue aux variables créer afin de les manipuler */
        mMessageConnection = (TextView) findViewById(R.id.activity_homepage_message);
        mDashboard = (Button) findViewById(R.id.activity_homepage_btn_dashboard);
        mOffer = (Button) findViewById(R.id.activity_homepage_btn_offre);
        mAccount = (Button) findViewById(R.id.activity_homepage_btn_account);
        mContact = (Button) findViewById(R.id.activity_homepage_btn_contact);
        mImageDYV = (ImageView) findViewById(R.id.activity_homepage_imageDYV);

        mOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent offerActivity = new Intent(HomePageActivity.this,OfferActivity.class);
                startActivity(offerActivity);
            }
        });

        mContact.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent contactActivity = new Intent(HomePageActivity.this,ContactActivity.class);
                startActivity(contactActivity);
            }
        });

    }
}
