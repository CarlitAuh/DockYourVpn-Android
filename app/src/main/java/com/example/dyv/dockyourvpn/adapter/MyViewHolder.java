package com.example.dyv.dockyourvpn.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dyv.dockyourvpn.R;
import com.example.dyv.dockyourvpn.model.Offer;

public class MyViewHolder extends RecyclerView.ViewHolder {

    private TextView mOfferName;
    private TextView mOfferPrice;

    public MyViewHolder(View itemView){
        super(itemView);

        // On déclare notre find view de CELL_CARD
        mOfferName = (TextView) itemView.findViewById(R.id.nameOffer);
        mOfferPrice = (TextView) itemView.findViewById(R.id.priceOffer);
    }

    // Fonction qui remplit la cellule en fonction de l'objet
    public void bind(Offer myOffer){
        mOfferName.setText(myOffer.getName());
        mOfferPrice.setText(myOffer.getPrice());
    }
}
