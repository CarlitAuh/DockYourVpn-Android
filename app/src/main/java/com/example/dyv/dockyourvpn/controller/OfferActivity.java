package com.example.dyv.dockyourvpn.controller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.dyv.dockyourvpn.R;
import com.example.dyv.dockyourvpn.adapter.OfferAdapter;
import com.example.dyv.dockyourvpn.model.Offer;

import java.util.ArrayList;
import java.util.List;

public class OfferActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private List<Offer> mOfferList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer);

        addOffer();

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        //définit l'agencement des cellules, ici de façon verticale, comme une ListView
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        //pour adapter en grille comme une RecyclerView, avec 2 cellules par ligne
        //recyclerView.setLayoutManager(new GridLayoutManager(this,2));

        mRecyclerView.setAdapter(new OfferAdapter(mOfferList));

    }

    private void addOffer(){
        Offer offerLowCost = new Offer();
        offerLowCost.setName("MicroOffre");
        offerLowCost.setPrice("15e");
        offerLowCost.setDescription("Ben tu n'as pas accès à tout c'est con !");

        Offer offerMedium = new Offer();
        offerMedium.setName("Offre Moyenne");
        offerMedium.setPrice("30e");
        offerMedium.setDescription("Boooooof !");

        mOfferList.add(offerLowCost);
        mOfferList.add(offerMedium);
    }
}
