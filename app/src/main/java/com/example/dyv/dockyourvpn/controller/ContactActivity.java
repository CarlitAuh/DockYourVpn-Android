package com.example.dyv.dockyourvpn.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.dyv.dockyourvpn.R;

public class ContactActivity extends AppCompatActivity {


    private TextView mMessageContact;
    private EditText mMailContact;
    private EditText mObjectContact;
    private EditText mRequestContact;
    private Button mSendContact;
    private TextView mCoordonneeContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        mMessageContact = (TextView) findViewById(R.id.activity_contact_message);
        mMailContact = (EditText) findViewById(R.id.activity_contact_mail);
        mObjectContact = (EditText) findViewById(R.id.activity_contact_object);
        mRequestContact = (EditText) findViewById(R.id.activity_contact_request);
        mSendContact = (Button) findViewById(R.id.activity_contact_send);
        mCoordonneeContact = (TextView) findViewById(R.id.activity_contact_coordonnée);

        mSendContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"contact@dockyourvpn.ovh"});
                i.putExtra(Intent.EXTRA_SUBJECT, mObjectContact.getText().toString());
                i.putExtra(Intent.EXTRA_TEXT, mRequestContact.getText().toString());
                startActivity(Intent.createChooser(i, "Application Mail : "));
            }
        });
    }
}
